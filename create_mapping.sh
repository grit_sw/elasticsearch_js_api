#!/usr/bin/env bash

source .env;

# Enable the terminal support to pass on errors, kinda like an automatic try catch 
set -e ;

# curl -X DELETE $ES_HOST:$ES_PORT/$ES_INDEX;

# curl -X DELETE $ES_HOST:$ES_PORT/_template/*;

curl -XPUT $ES_HOST:$ES_PORT/meter-stream -H "Content-Type: application/json" -d @mapping.json;

# curl -XPUT $ES_HOST:$ES_PORT/_template/new- -H "Content-Type: application/json" -d @mapping.json;

