# NAME #
elasticsearch_js_api

# LANGUAGE #
JAVASCRIPT (NODEJS)

### INSTRUCTIONS ###
The following steps are required for setting up the project

# Required Software #
* npm/yarn
* nodejs (v8.12.0 and above) 
* elasticsearch
* mongodb
* docker (optional)
* docker-compose (optional)

# How do I get set up [without docker]? #
* `cd elasticsearch_js_api`
* `git checkout stable`
* `npm i`
* `sh create_dir_logs.sh` (for production, testing purposes only on local dev env)
* `cp .env.sample .env` (update .env appropriately)
* if working with confluent/simulator, create mapping for index manually before running simulator to avoid mapping errors `sh create_mapping.sh` (optional, don't ever run this in the production env)
* `npm run watch` (to watch for changes during development, refer to package.json for production build)

# How do I get set up [with docker]? #
* `cd elasticsearch_js_api`
* `git checkout stable`
* `cp .env.sample .env` (update .env appropriately)
* if working with confluent/simulator, create mapping for index manually before running simulator to avoid mapping errors `sh create_mapping.sh` (optional, don't ever run this in the production env)
* To build and run development image as container, execute the commands below
* `docker build -t node-api:dev-latest .` 'node-api:prod-latest' for production
* `docker run -d -it --name node-api -p 3000:3000 node-api` 
* The 2 commands above is accomplished with docker-compose by executing `docker-compose up --build`

# Resources/Links #
* https://github.com/creationix/nvm#install-script nvm to better manage node/npm 
* https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html
* https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/quick-start.html#_elasticsearch_query_dsl
* https://www.elastic.co/guide/en/elasticsearch/guide/master/_coping_with*failure.html
* https://elastic-builder.js.org/docs/
* https://www.digitalocean.com/community/tutorials/how-to-set-up-a-production-elasticsearch-cluster-on-ubuntu-14-04
* There's always Google and Stackoverflow

# Who do I talk to? #
* Moyo
* Faith
