const createError = require('http-errors');
const express = require('express');
// const cors = require('cors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const config = require('config');
const debug = require('debug')('es:app');

const app = express();

if (config.env === 'development') {
  debug(config.env)
  app.use(logger('dev'));
}
else if (config.env === 'production') {
  debug(config.env)
  app.use(logger('common'));
}
app.use(express.json());
// app.use(cors());
app.use(express.urlencoded({ extended: false }));

/**
 * Middleware to handle preflight and main requests allowing for CORS
 */
// app.all(/\//, function (req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', config.es.allowedOrigin);
//   res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
//   next();
// });
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/**
 * Get and set all API routes and handlers by config id
 */
app.use('/daily', configRoute('dailyRouter'));
app.use('/recent', configRoute('recentRouter'));
app.use('/data', configRoute('dataRouter'));
app.use('/cmdctrl', configRoute('cmdctrlRouter'));
app.use('/monthly', configRoute('monthlyRouter'));
app.use('/bounds', configRoute('boundsRouter'));
app.use('/exportToCsv', configRoute('exportToCsvRouter'));
app.use('/yearly', configRoute('yearlyRouter')); // Currently not in use
// app.use('/consumerGroup', configRoute('consumerGroupRouter'));
// app.use('/consumer', configRoute('consumerRouter'));

/**
 * Get and set all API routes and handlers by probe id
 */
app.use('/probe/daily', probeRoute('dailyRouter'));
app.use('/probe/recent', probeRoute('recentRouter'));
app.use('/probe/data', probeRoute('dataRouter'));
app.use('/probe/cmdctrl', probeRoute('cmdctrlRouter'));
app.use('/probe/monthly', probeRoute('monthlyRouter'));
app.use('/probe/bounds', probeRoute('boundsRouter'));
app.use('/probe/exportToCsv', probeRoute('exportToCsvRouter'));
app.use('/probe/yearly', probeRoute('yearlyRouter')); // Currently not in use
// app.use('/probe/consumerGroup', probeRoute('consumerGroupRouter'));
// app.use('/probe/consumer', probeRoute('consumerRouter'));

/**
 * Catch and handle 404 Error
 */
app.use(function (req, res) {
  const err = new createError.NotFound();

  res.status(err.status || 500).json({
    'error_code': err.status,
    'message': err.message
  });
});

module.exports = app;
