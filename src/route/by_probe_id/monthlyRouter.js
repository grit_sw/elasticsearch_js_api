const router = require('express').Router();
const debug = require('debug')('route:monthly');
const monthly = probeModel('monthly');

/* GET monthly records. */
router.get('/:probeID/:startTime/:stopTime', function (req, res) {
    monthly(req.params.probeID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data)
    })
    .catch(err => {
      debug(err)
      res.json({ error: err, message: 'failed to connect' });
    });
});

module.exports = router;
