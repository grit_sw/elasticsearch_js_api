const router = require('express').Router();
const debug = require('debug')('route:data');
const data = probeModel('data');

/* GET data records. */
router.get('/:probeID/:startTime/:stopTime', function (req, res) {
  data(req.params.probeID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err)
      res.status(504).json(err);
    });
});

module.exports = router;
