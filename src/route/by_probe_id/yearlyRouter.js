const router = require('express').Router();
const debug = require('debug')('route:yearly');
const yearly = probeModel('yearly');

/* GET yearly records. */
router.get('/:probeID/:startTime/:stopTime', function (req, res) {
  yearly(req.params.probeID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err)
      res.json(err);
    });
});

module.exports = router;
