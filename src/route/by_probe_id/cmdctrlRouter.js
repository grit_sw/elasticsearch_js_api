const router = require('express').Router();
const debug = require('debug')('route:cmdctrl');
const cmdctrl = probeModel('cmdctrl');

/* GET cmdctrl records. */
router.get('/:probeID/:startTime/:stopTime', function (req, res) {
  cmdctrl(req.params.probeID, req.params.startTime, req.params.stopTime)
    .then(totalHits => {
      res.json(totalHits);
    })
    .catch(err => {
      debug
      res.json(err);
    });
});

module.exports = router;
