const {client, esb, moment} = probeModel('global');
const { consumerGroup } = probeModel('consumerGroup');

const router = require('express').Router();

/* GET consumerGroup records. */
router.get('/:probeID/:startTime/:stopTime', function (req, res, next) {
  consumerGroup(client, esb, moment, req.params.probeID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.send(err);
    });
});

module.exports = router;
