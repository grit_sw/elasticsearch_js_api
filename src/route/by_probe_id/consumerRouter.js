const {client, esb, moment} = probeModel('global');
const { consumer } = probeModel('consumer');
const config = require('config');

const router = require('express').Router();

/* GET consumer records. */
router.get('/:probeID/:startTime/:stopTime', function (req, res, next) {
  consumer(client, esb, moment, req.params.probeID, req.params.startTime, req.params.stopTime, config)
    .then(data => {
      res.send(data);
    }).catch(err => {
      console.log(err)
      res.send(err);
    });
});

module.exports = router;
