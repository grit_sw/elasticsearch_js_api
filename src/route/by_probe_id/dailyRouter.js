const router = require('express').Router();
const debug = require('debug')('route:daily');
const daily = probeModel('daily');

/* GET daily records. */
router.get('/:probeID/:startTime/:stopTime', function (req, res) {
  daily(req.params.probeID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err);
      res.json(err);
    });
});

module.exports = router;
