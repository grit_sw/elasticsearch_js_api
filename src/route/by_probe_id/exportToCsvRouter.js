const router = require('express').Router();
const debug = require('debug')('route:exportToCsv');
const exportToCsv = probeModel('exportToCsv');

/* GET exportToCsv records. */
router.get('/:probeID/:startTime/:stopTime/:intervalValue/:intervalUnit', function (req, res) {
  exportToCsv(req.params.probeID, req.params.startTime, req.params.stopTime, req.params.intervalValue, req.params.intervalUnit)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err);
      res.json({msg: err});
    });
});

module.exports = router;
