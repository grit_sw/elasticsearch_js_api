const router =  require('express').Router();
const debug = require('debug')('route:bounds');
const bounds = probeModel('bounds');

/* GET bounds records. */
router.get('/:probeID', function (req, res) {
  bounds(req.params.probeID)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug()
      res.json(err);
    });
});

module.exports = router;
