const router = require('express').Router();
const debug = require('debug')('route:monthly');
const monthly = configModel('monthly');

/* GET monthly records. */
router.get('/:confID/:startTime/:stopTime', function (req, res) {
    monthly(req.params.confID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data)
    })
    .catch(err => {
      debug(err)
      res.json({ error: err, message: 'failed to connect' });
    });
});

module.exports = router;
