const router = require('express').Router();
const debug = require('debug')('route:yearly');
const yearly = configModel('yearly');

/* GET yearly records. */
router.get('/:confID/:startTime/:stopTime', function (req, res) {
  yearly(req.params.confID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err)
      res.json(err);
    });
});

module.exports = router;
