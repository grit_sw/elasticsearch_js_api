const router = require('express').Router();
const debug = require('debug')('route:data');
const data = configModel('data');

/* GET data records. */
router.get('/:configID/:startTime/:stopTime', function (req, res) {
  data(req.params.configID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err)
      res.status(504).json(err);
    });
});

module.exports = router;
