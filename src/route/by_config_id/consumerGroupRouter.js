const {client, esb, moment} = configModel('global');
const { consumerGroup } = configModel('consumerGroup');

const router = require('express').Router();

/* GET consumerGroup records. */
router.get('/:configID/:startTime/:stopTime', function (req, res, next) {
  consumerGroup(client, esb, moment, req.params.configID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.send(err);
    });
});

module.exports = router;
