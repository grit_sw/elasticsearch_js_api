const router = require('express').Router();
const debug = require('debug')('route:daily');
const daily = configModel('daily');

/* GET daily records. */
router.get('/:configID/:startTime/:stopTime', function (req, res) {
  daily(req.params.configID, req.params.startTime, req.params.stopTime)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err);
      res.json(err);
    });
});

module.exports = router;
