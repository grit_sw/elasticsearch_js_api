const {client, esb, moment} = configModel('global');
const { consumer } = configModel('consumer');
const config = require('config');

const router = require('express').Router();

/* GET consumer records. */
router.get('/:configID/:startTime/:stopTime', function (req, res, next) {
  consumer(client, esb, moment, req.params.configID, req.params.startTime, req.params.stopTime, config)
    .then(data => {
      res.send(data);
    }).catch(err => {
      console.log(err)
      res.send(err);
    });
});

module.exports = router;
