const router = require('express').Router();
const debug = require('debug')('route:exportToCsv');
const exportToCsv = configModel('exportToCsv');

/* GET exportToCsv records. */
router.get('/:configID/:startTime/:stopTime/:intervalValue/:intervalUnit', function (req, res) {
  exportToCsv(req.params.configID, req.params.startTime, req.params.stopTime, req.params.intervalValue, req.params.intervalUnit)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err);
      res.json({msg: err});
    });
});

module.exports = router;
