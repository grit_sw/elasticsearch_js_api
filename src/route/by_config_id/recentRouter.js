const router = require('express').Router();
const debug = require('debug')('route:recent');
const recent = configModel('recent');

/* GET recent records. */
router.get('/:configID/:stopTime/:window', function (req, res) {
  recent(req.params.configID, req.params.stopTime, req.params.window)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug(err)
      res.json(err);
    });
});

module.exports = router;
