const router =  require('express').Router();
const debug = require('debug')('route:bounds');
const bounds = configModel('bounds');

/* GET bounds records. */
router.get('/:configID', function (req, res) {
  bounds(req.params.configID)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      debug()
      res.json(err);
    });
});

module.exports = router;
