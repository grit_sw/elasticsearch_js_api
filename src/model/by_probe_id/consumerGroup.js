module.exports.consumerGroup = function (client, esb, moment, probeID, startISO, stopISO) {

  const startDate = moment(startISO);
  const stopDate = moment(stopISO);

  /**
   * get the User time zone offset
   * Changes the user time zone based on location to the numerical format
   * e.g - Africa/Lagos  -> +01:00
   */
  const userTimeZone = moment.tz.guess()
  const userTimeZoneOffset = startDate.tz(userTimeZone).format('Z');


  /**
   * This appends the timezone to the date
   * i.e - 2018-07-04T01:31:37.345 -> 2018-07-04T01:31:37.345+01:00
   */
  const startDateWithTimeZone = startDate.tz(userTimeZone);
  const stopDateWithTimeZone = stopDate.tz(userTimeZone);


  /**
   * Difference  between both times in seconds
   */
  const secsInFrame = (stopDate - startDate) / 1000;

  /**
   * The interval must always be greater than 5 seconds
   * If less than 5 seconds, set the interval to 5 seconds either way
   */
  const maxDataPoints = process.env.MAX_DATA_POINTS;
  const histogramInterval = secsInFrame < process.env.TIMING_INT ? process.env.TIMING_INT : secsInFrame / maxDataPoints;

  /**
   * elasticsearch-builder query that generates the initial query for matches
   */
  const baseQuery = esb.boolQuery()
    .must(esb.matchQuery('master.id', probeID))
    .filter(esb.rangeQuery('master.time').gt(startDateWithTimeZone).lt(stopDateWithTimeZone));

  /**
   * Nested Aggregration for Power hits
   */
  const aggPower = esb.nestedAggregation('Power_', 'data')
    .agg(esb.dateHistogramAggregation('histogram', 'data.time', histogramInterval)
      .timeZone(userTimeZoneOffset)
      .agg(esb.nestedAggregation('activeSource', 'data.activeSource')
        .agg(esb.termsAggregation('sourceType', 'data.activeSource.sourceType'))
      )
      .agg(esb.nestedAggregation('powerNest', 'data.powerSinceLast')
        .agg(esb.termsAggregation('configID', 'data.powerSinceLast.configID_FK'))
        .agg(esb.termsAggregation('sourceType', 'data.powerSinceLast.sourceType'))
        .agg(esb.statsAggregation('power', 'data.powerSinceLast.powerSinceLast'))
      )
    )

  /**
   * Final elasticsearch-builder session to generate query to be used in making es request
   */
  const query = esb.requestBodySearch()
    .query(baseQuery)
    .agg(aggPower)
    .sort(esb.sort('master.time', 'asc'));

  /**
   * Make search and display/return data
   */
  return client.search({
    index: process.env.ES_INDEX,
    type: process.env.ES_TYPE,
    body: query.toJSON(),
    requestTimeout: process.env.REQUEST_TIMEOUT
  }).then(res => {
    // console.log(JSON.stringify(res));
    return res;
  }).catch(err => {
    // console.trace(err.message);
    return err;
  });

  client.close();

};