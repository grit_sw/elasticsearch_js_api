const moment = require('moment-timezone');
const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:monthly');

const client = require('../client');
const DB = require('../db');

let displayEnergyToday;

// Connect to mongo databse and fetch needed item
DB.connect()
  .then(db => {
    return db.collection('configurations').findOne({ _id: 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF' })
  })
  .then(data => {
    displayEnergyToday = data.DisplayEnergyToday;
  })
  .catch(err => debug(err))

module.exports = function (probeID, startISO, stopISO) {

  const startDate = moment(startISO);
  const stopDate = moment(stopISO);
  const timeZoneOffset = moment().tz('Africa/Lagos').format('Z');
  // const timeZoneOffset = '+01:00';

  /**
   * elastic-builder query that generates the initial query for matches
   */
  const baseQuery = esb.boolQuery()
    .must(esb.matchQuery('master.id', probeID))
    .filter(esb.rangeQuery('master.time').gt(startDate).lt(stopDate));

  /**
   * Nested Aggregration for Energy hits
   */
  var aggs;
  switch (displayEnergyToday) {
    case true:
      debug('displayEnergyToday:', displayEnergyToday);
      aggs = esb.nestedAggregation('EnergyDay_', 'data')
        .agg(esb.dateHistogramAggregation('histogram', 'data.time', 'day')
          .timeZone(timeZoneOffset)
          .agg(esb.topHitsAggregation('topHits')
            .size(3)
            .sort(esb.sort('data.time', 'desc'))
          )
        )
      break;

    case false:
      debug('displayEnergyToday:', displayEnergyToday);
      aggs = esb.nestedAggregation('EnergyDay_', 'data')
        .agg(esb.dateHistogramAggregation('histogram', 'data.time', 'day')
          .timeZone(timeZoneOffset)
          .agg(esb.nestedAggregation('energyNest', 'data.energySinceLast')
            .agg(esb.termsAggregation('configID', 'data.energySinceLast.configID_FK.keyword')
              .agg(esb.termsAggregation('SourceType', 'data.energySinceLast.sourceType.keyword')
                .agg(esb.statsAggregation('energy', 'data.energySinceLast.energySinceLast'))
              )
            )
          )
          .agg(esb.nestedAggregation('costNest', 'data.costSinceLast')
            .agg(esb.termsAggregation('configID', 'data.costSinceLast.configID_FK.keyword')
              .agg(esb.termsAggregation('SourceType', 'data.costSinceLast.sourceType.keyword')
                .agg(esb.statsAggregation('cost', 'data.costSinceLast.costSinceLast'))
              )
            )
          )
          .agg(esb.nestedAggregation('timeToday', 'data.timeTodaySource')
            .agg(esb.termsAggregation('configID', 'data.timeTodaySource.configID_FK.keyword')
              .agg(esb.termsAggregation('SourceType', 'data.timeTodaySource.sourceType.keyword')
                .agg(esb.reverseNestedAggregation('dataHits')
                  .agg(esb.topHitsAggregation('topHits')
                    .size(3)
                    .sort(esb.sort('data.time', 'desc'))
                    // .nestedPath('data')
                  )
                )
              )
            )
          )
        )
      break;
  }

  /**
   * Final elastic-builder session to generate query to be used in making es request
   */
  const query = esb.requestBodySearch()
    .query(baseQuery)
    .agg(aggs)
    .sort(esb.sort('master.time', 'asc'));

  /**
   * Make search and display/return data
   */
  return client.search({
    index: config.es.index,
    type: config.es.type,
    search_type: 'dfs_query_then_fetch',
    body: query.toJSON(),
    requestTimeout: config.es.requestTimeout
  })
    .then(res => {
      debug(res.hits.hits.length);
      res.DisplayEnergyToday = displayEnergyToday; // Add to base object
      return res;
    })
    .catch(err => {
      debug(err.message);
      return err;
    });

};
