const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:bounds');

const client = require('../client');

module.exports = function (probeID) {

  /**
   * elasticsearch-builder query that generates the initial query for matches
   */
  const baseQuery = esb.matchQuery('master.id', probeID);

  /**
   * Final elasticsearch-builder session to generate query to be used in making es request
   */
  const query_asc = esb.requestBodySearch()
    .query(baseQuery)
    .size(1)
    .sort(esb.sort('master.time', 'asc'));

  const query_desc = esb.requestBodySearch()
    .query(baseQuery)
    .size(1)
    .sort(esb.sort('master.time', 'desc'));

  /**
   * Execute search and return data
   */
  return Promise.all([
    client.search({
      index: config.es.index,
      type: config.es.type,
      body: query_asc.toJSON(),
      requestTimeout: config.es.requestTimeout
    }).then(res => {
      debug(JSON.stringify(new Date(res.hits.hits[0]._source.master.time).getTime()));
      return JSON.stringify(new Date(res.hits.hits[0]._source.master.time).getTime());
    }),

    client.search({
      index: config.es.index,
      type: config.es.type,
      body: query_desc.toJSON(),
      requestTimeout: config.es.requestTimeout
    }).then(res => {
      debug(JSON.stringify(new Date(res.hits.hits[0]._source.master.time).getTime()));
      return JSON.stringify(new Date(res.hits.hits[0]._source.master.time).getTime());
    })
  ])
    .then(res => {
      return {
        start: res[0],
        stop: res[1]
      }
    })
    .catch(err => {
      debug(err)
      return err.message;
    })

};