const moment = require('moment-timezone');
const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:yearly');

const client = require('../client');

module.exports = function (probeID, startISO, stopISO) {
  const startDate = moment(startISO);
  const stopDate = moment(stopISO);
  const timeZoneOffset = moment().tz('Africa/Lagos').format('Z');
  // const timeZoneOffset = '+01:00';  

  /**
   * elastic-builder query that generates the initial query for matches
   */
  const baseQuery = esb.nestedQuery().path('data')
    .query(esb.rangeQuery('data.time').gt(startDate).lt(stopDate));

  /**
   * Nested Aggregration for Energy Year hits. Currently not in use
   */
  const agg16 = esb.dateHistogramAggregation('EnergyYear', 'time', 'month')
    // .timezone(timeZoneOffset)
    .agg(esb.termsAggregation('SourceType', 'SourceType')
      .agg(esb.statsAggregation('CostStats', 'Cost_since_last'))
      .agg(esb.statsAggregation('energyStats', 'Energy_since_last'))
    )
    .agg(esb.statsAggregation('energy', 'Energy_since_last'))
    .agg(esb.statsAggregation('TimeStats', 'Cost_since_last'))

  //   const agg16 = esb.dateHistogramAggregation('EnergyYear', 'time', 'month')
  //   // .timezone(timeZoneOffset)
  //   .agg(esb.termsAggregation('SourceType', 'SourceType')
  //     .agg(esb.statsAggregation('CostStats', 'data.costSinceLast.costSinceLast'))
  //     .agg(esb.statsAggregation('energyStats', 'data.energySinceLast.energySinceLast'))
  //   )
  //   .agg(esb.statsAggregation('energy', 'data.energySinceLast.energySinceLast'))
  //   .agg(esb.statsAggregation('TimeStats', 'data.costSinceLast.costSinceLast'))

  /**
   * Nested Aggregration for Energy hits
   */
  const aggs = esb.nestedAggregation('EnergyDay_', 'data')
    .agg(esb.dateHistogramAggregation('histogram', 'data.time', 'day')
      .timeZone(timeZoneOffset)
      .agg(esb.topHitsAggregation('topHits')
        .size(1)
        .sort(esb.sort('data.time', 'desc'))
      )
    );

  /**
   * Final elastic-builder session to generate query to be used in making es request
   */
  const query = esb.requestBodySearch()
    .query(baseQuery)
    // .agg(agg16) // Currently not in use
    .agg(aggs)
    .sort(esb.sort('master.time', 'desc'));

  /**
   * Execute search and return data
   */
  return client.search({
    index: config.es.index,
    type: config.es.type,
    search_type: 'dfs_query_then_fetch',
    body: query.toJSON(),
    requestTimeout: config.es.requestTimeout
  })
    .then(res => {
      debug(res.hits.hits.length);
      return res;
    })
    .catch(err => {
      debug(err.message);
      return err;
    });

};