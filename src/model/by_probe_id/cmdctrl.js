const moment = require('moment-timezone');
const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:cmdctrl');

const client = require('../client');

module.exports = function (probeID, startISO, stopISO) {

  const startDate = moment(startISO);
  const stopDate = moment(stopISO);

  /**
   * elasticsearch-builder query that generates the initial query for matches
   */
  const baseQuery = esb.boolQuery()
    .must(esb.matchQuery('master.id', probeID))
    .filter(esb.rangeQuery('master.time').gt(startDate).lt(stopDate));

  /**
   * Final elasticsearch-builder session to generate query to be used in making es request
   */
  const query = esb.requestBodySearch().query(baseQuery);

  /**
   * Execute search and return data
   */
  return client.search({
    index: config.esindex,
    type: config.es.type,
    body: query.toJSON(),
    requestTimeout: config.es.requestTimeout
  })
    .then(res => {
      debug(res.hits.hits.length);
      return res.hits.total
    })
    .catch(err => {
      debug(err.message);
      return err;
    });
};