const moment = require('moment-timezone');
const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:daily');

const client = require('../client');

module.exports = function (configID, startISO, stopISO) {

  /**
   * Allow for easy datetime targetting
   */
  const startDate = moment(startISO);
  const stopDate = moment(stopISO);
  const timeZoneOffset = moment().tz('Africa/Lagos').format('Z');
  // const timeZoneOffset = '+01:00';

  /**
   * Difference  between both times in seconds
   */
  const secsInFrame = (stopDate - startDate) / 1000;

  /**
   * The interval must always be greater than 5 seconds
   * If less than 5 seconds, set the interval to 5 seconds either way
   */
  const maxDataPoints = config.es.vars.MAX_DATA_POINTS;
  const dateHistogramInterval = secsInFrame <= config.es.vars.TIMING_INT ? config.es.vars.TIMING_INT : parseInt(secsInFrame / maxDataPoints);

  /**
   * elasticsearch-builder query that generates the initial query for matches
   */
  const baseQuery = esb.boolQuery()
    .must(esb.matchQuery('master.configuration_IDs', configID))
    .filter(esb.rangeQuery('master.time').gt(startDate).lt(stopDate));

  /**
   * Nested Aggregration for Power hits
   */
  const aggPower = esb.nestedAggregation('Power_', 'data')
    .agg(esb.dateHistogramAggregation('histogram', 'data.time', `${dateHistogramInterval}s`)
      .timeZone(timeZoneOffset)
      .agg(esb.nestedAggregation('activeSource', 'data.activeSource')
        .agg(esb.termsAggregation('sourceType', 'data.activeSource.sourceType.keyword'))
      )
      .agg(esb.nestedAggregation('powerNest', 'data.powerSinceLast')
        .agg(esb.termsAggregation('configID', 'data.powerSinceLast.configID_FK.keyword'))
        .agg(esb.termsAggregation('SourceType', 'data.powerSinceLast.sourceType.keyword'))
        .agg(esb.statsAggregation('power', 'data.powerSinceLast.powerSinceLast'))
      )
    )

  /**
   * Final elasticsearch-builder session to generate query to be used in making es request
   */
  const query = esb.requestBodySearch()
    .query(baseQuery)
    .agg(aggPower)
    .sort(esb.sort('master.time', 'asc'));

  /**
   * Execute search and return data
   */
  return client.search({
    index: config.es.index,
    type: config.es.type,
    body: query.toJSON(),
    requestTimeout: config.es.requestTimeout
  })
    .then(res => {
      debug(res.hits.hits.length);
      return res;
    })
    .catch(err => {
      debug(err.message);
      return err;
    });

};