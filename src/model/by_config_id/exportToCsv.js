const moment = require('moment-timezone');
const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:exportToCsv');

const client = require('../client');

function exportAgrregations(configID, startISO, stopISO, interval) {

    const startDate = moment(startISO);
    const stopDate = moment(stopISO);
    const timeZoneOffset = moment().tz('Africa/Lagos').format('Z');    
    // const timeZoneOffset = '+01:00';

    /**
     * esb query that generates the initial query for matches
     */
    const baseQuery = esb.boolQuery()
        .must(esb.matchQuery('master.configuration_IDs', configID))
        .filter(esb.rangeQuery('master.time').gt(startDate).lt(stopDate));

    /**
     * Aggregations for the Power data
     */
    const aggs = esb.nestedAggregation('Power_', 'data')
        .agg(esb.dateHistogramAggregation('histogram', 'data.time', interval)
            .timeZone(timeZoneOffset)
            .agg(esb.nestedAggregation('powerNest', 'data.powerSinceLast')
                .agg(esb.termsAggregation('configID', 'data.powerSinceLast.configID_FK.keyword')
                    .agg(esb.termsAggregation('SourceName', 'data.powerSinceLast.sourceName.keyword')
                        .agg(esb.statsAggregation('powerStats', 'data.powerSinceLast.powerSinceLast'))
                    )
                )
            )
            .agg(esb.topHitsAggregation('topHits')
                .size(1)
                .sort(esb.sort('data.time', 'desc'))
            )
        );

    /**
     * Final elasticsearch-builder session to generate query to be used in making es request
     */
    const query = esb.requestBodySearch()
        .query(baseQuery)
        .agg(aggs)
        .sort(esb.sort('master.time', 'asc'));

    /**
     * Make search and display/return needed data
     */
    return client.search({
        index: config.es.index,
        type: config.es.type,
        search_type: 'dfs_query_then_fetch',
        body: query.toJSON(),
        requestTimeout: config.es.requestTimeout
    })
        .then(res => {
            debug(res.hits.hits.length);
            return res;
        })
        .catch(err => {
            debug(err.message);
            return err;
        });
        
}

function scrollExport(configID, startISO, stopISO, interval) {
    const startDate = moment(startISO);
    const stopDate = moment(stopISO);
    const timeZoneOffset = moment().tz('Africa/Lagos').format('Z');    
    /**
     * esb query that generates the initial query for matches
     */
    const baseQuery = esb.boolQuery()
        .must(esb.matchQuery('master.configuration_IDs', configID))
        .filter(esb.rangeQuery('master.time').gt(startDate).lt(stopDate));

    /**
     * Aggregations for the Power data
     */
    const aggs = esb.nestedAggregation('Power_', 'data')
        .agg(esb.dateHistogramAggregation('histogram', 'data.time', interval)
            .timeZone(timeZoneOffset)
            .agg(esb.nestedAggregation('powerNest', 'data.powerSinceLast')
                .agg(esb.termsAggregation('configID', 'data.powerSinceLast.configID_FK.keyword')
                    .agg(esb.termsAggregation('SourceName', 'data.powerSinceLast.sourceName.keyword')
                        .agg(esb.statsAggregation('powerStats', 'data.powerSinceLast.powerSinceLast'))
                    )
                )
            )
            .agg(esb.topHitsAggregation('topHits')
                .size(1)
                .sort(esb.sort('data.time', 'desc'))
            )
        );

    /**
     * Final elasticsearch-builder session to generate query to be used in making es request
     */
    const query = esb.requestBodySearch()
        .query(baseQuery)
        .agg(aggs)
        .sort(esb.sort('master.time', 'asc'));

        return client.search({
            index: config.es.index,
            type: config.es.type,
            scroll: '10s',
            body: query.toJSON()
        }
        ).then(response => {
            return response;
        }
        ).catch(e => {
            console.log(e);
            return e;
        });
    }

module.exports = function (configID, startISO, stopISO, intervalValue, intervalUnit) {
    const interval = parseInt(intervalValue);
    var dateHistogramInterval;

    switch (intervalUnit) {
        case 'Minute':
            dateHistogramInterval = interval + 'm';
            response = scrollExport(configID, startISO, stopISO, dateHistogramInterval);
            return response;
            // break;
        case 'Hour':
            dateHistogramInterval = interval + 'h';
            break;
        case 'Day':
            dateHistogramInterval = interval + 'd';
            break;
        case 'Week':
            dateHistogramInterval = 'week';
            break;
        case 'Month':
            dateHistogramInterval = 'month';
            break;
        case 'Year':
            dateHistogramInterval = 'year';
            break;
        default:
            return Promise.reject('Incorrect intervalUnit')
            break;
    }

    return exportAgrregations(configID, startISO, stopISO, dateHistogramInterval);
};