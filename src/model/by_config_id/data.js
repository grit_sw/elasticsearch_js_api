const moment = require('moment-timezone');
const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:data');

const client = require('../client');

module.exports = function (configID, startISO, stopISO) {

    /**
     * Allow for easy datetime targetting
     */
    const startDate = moment(startISO);
    const stopDate = moment(stopISO);
    const timeZoneOffset = moment().tz('Africa/Lagos').format('Z');    
    // const timeZoneOffset = '+01:00';

    /**
     * Difference  between both times in milliseconds
     */
    const delta = (stopDate - startDate);

    /**
     * The interval must always be greater than 5 seconds
     * If less than 5 seconds, set the interval to 5 seconds either way
     */
    let interval = (delta / config.es.vars.MAX_HISTOGRAM_BARS) / 1000;
    interval = interval >= config.es.vars.TIMING_INT ? parseInt(interval) : config.es.vars.TIMING_INT;

    /**
     * Elasticsearch sets the interval using normal time literals
     * e.g s -> seconds e.g '5s'
     * We are using a least interval of 5 seconds for sampling
     * Check the value in the config files -> config.es.vars.TIMING_INT 
     */
    // const dateHistogramInterval = `${interval}ms`;
    const dateHistogramInterval = `${interval}s`;

    // const docsPerBucket = interval / config.es.vars.TIMING_INT; // Currently not in use

    /**elasticsearch-builder query that generates the initial query for matches*/
    const baseQuery = esb.boolQuery()
        .must(esb.matchQuery('master.configuration_IDs', configID))
        .filter(esb.rangeQuery('master.time').gt(startDate).lt(stopDate));

    /**
     * Nested Aggregration for Energy hits
     */
    const aggs = esb.nestedAggregation('EnergyDay_', 'data')
        .agg(esb.dateHistogramAggregation('histogram', 'data.time', 'day')
            .timeZone(timeZoneOffset)
            .agg(esb.topHitsAggregation('topHits')
                .size(3)
                .sort(esb.sort('data.time', 'desc'))
            )
        );

    /**
     * Aggregations for the fuel hour rate data
     */
    const aggFuelRate = esb.nestedAggregation('fuelRate', 'data')
        .agg(esb.dateHistogramAggregation('fuelRateHistogram', 'data.time', dateHistogramInterval)
            .minDocCount(0)
            .extendedBounds((new Date(startDate)), (new Date(stopDate)))
            .timeZone(timeZoneOffset)
            .agg(esb.nestedAggregation('fuelRateNest', 'data.fuelHourRate')
                .agg(esb.termsAggregation('configID', 'data.fuelHourRate.configID_FK.keyword')
                    .agg(esb.termsAggregation('SourceName', 'data.fuelHourRate.sourceName.keyword')
                        .agg(esb.statsAggregation('fuelRate', 'data.fuelHourRate.fuelHourRate'))
                    )
                )
            )
        );

    /**
     * Aggregations for the fuel since last data
     */
    const aggFuel = esb.nestedAggregation('fuel', 'data')
        .agg(esb.dateHistogramAggregation('fuelHistogram', 'data.time', dateHistogramInterval)
            .minDocCount(0)
            .extendedBounds((new Date(startDate)), (new Date(stopDate)))
            .timeZone(timeZoneOffset)
            .agg(esb.nestedAggregation('fuelNest', 'data.fuelSinceLast')
                .agg(esb.termsAggregation('configID', 'data.fuelSinceLast.configID_FK.keyword')
                    .agg(esb.termsAggregation('SourceName', 'data.fuelSinceLast.sourceName.keyword')
                        .agg(esb.statsAggregation('fuelStats', 'data.fuelSinceLast.fuelSinceLast'))
                    )
                )
            )
        );


    /**
     * Aggregations for the battery capacity since last data
     */
    const aggBatteryCap = esb.nestedAggregation('batteryCap', 'data')
        .agg(esb.dateHistogramAggregation('batteryCapHistogram', 'data.time', dateHistogramInterval)
            .timeZone(timeZoneOffset)
            .agg(esb.nestedAggregation('batteryNest', 'data.batteryCapLast')
                .agg(esb.termsAggregation('configID', 'data.batteryCapLast.configID_FK.keyword')
                    .agg(esb.termsAggregation('SourceName', 'data.batteryCapLast.sourceName.keyword')
                        .agg(esb.statsAggregation('batteryStats', 'data.batteryCapLast.batteryCapLast'))
                    )
                )
            )
        );

    /**
     * Aggregations for the battery dod data
     */
    const aggBatteryDOD = esb.nestedAggregation('batteryDOD', 'data')
        .agg(esb.dateHistogramAggregation('DODHistogram', 'data.time', dateHistogramInterval)
            .timeZone(timeZoneOffset)
            .agg(esb.nestedAggregation('DODNest', 'data.dod')
                .agg(esb.termsAggregation('configID', 'data.dod.configID_FK.keyword')
                    .agg(esb.termsAggregation('SourceName', 'data.dod.sourceName.keyword')
                        .agg(esb.statsAggregation('dodStats', 'data.dod.dod'))
                    )
                )
            )
        );

    /**
     * Final elasticsearch-builder session to generate query to be used in making es search
     */
    const query = esb.requestBodySearch()
        .query(baseQuery)
        .agg(aggs)
        .agg(aggFuel)
        .agg(aggFuelRate)
        .agg(aggBatteryCap)
        .agg(aggBatteryDOD)
        .sort(esb.sort('master.time', 'desc'));

    /**
     * Execute search and return data
     */
    return client.search({
        index: config.es.index,
        type: config.es.type,
        search_type: 'dfs_query_then_fetch',
        body: query.toJSON(),
        requestTimeout: config.es.requestTimeout
    })
        .then(res => {
            debug(res.hits.hits.length);
            return res;
        })
        .catch(err => {
            debug(err);
            return err;
        });
};
