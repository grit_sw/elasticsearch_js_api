const moment = require('moment-timezone');
const esb = require('elastic-builder');
const config = require('config');
const debug = require('debug')('es:recent');

const client = require('../client');

module.exports = function (configID, stopISO, window) {

  const stopDate = moment(stopISO);
  const startDate = moment(new Date(stopDate - (parseInt(window) * 1000)));

  /**
   * elasticsearch-builder query that generates the initial query for matches
   */
  const baseQuery = esb.boolQuery()
    .must(esb.matchQuery('master.configuration_IDs', configID))
    .filter(esb.rangeQuery('master.time').gt(startDate).lt(stopDate));

  /**
   * Final elasticsearch-builder session to generate query to be used in making es request
   */
  const probesPerConfig = config.es.vars.PROBES_PER_CONFIG;
  const query = esb.requestBodySearch()
    .query(baseQuery)
    .from(0)
    .size(probesPerConfig * parseInt(window) / config.es.vars.TIMING_INT)
    .sort(esb.sort('master.time', 'asc'));

  /**
   * Execute search and return data
   */
  return client.search({
    index: config.es.index,
    type: config.es.type,
    body: query.toJSON(),
    requestTimeout: config.es.requestTimeout
  }).then(res => {
    debug(res.hits.hits.length);
    return res;
  }).catch(err => {
    debug(err.message);
    return err.message;
  });
};