const MongoClient = require('mongodb').MongoClient;
const config = require('config');
const debug = require('debug')('db:connection');

const dbHost = `${config.db.host}:${config.db.port}`;
const dbName = config.db.name;

module.exports = {
  connect: function () {
    return MongoClient.connect(dbHost, { useNewUrlParser: true })
      .then(db => {
        debug('Database connection successful');
        return db.db(dbName);
      })
      .catch(err => {
        debug('Error: ' + err);
        return err;
      })
  }
}