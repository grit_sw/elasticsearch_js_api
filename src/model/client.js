/**
 * Initialize client to be used in making all Elasticsearch requests
 */

const config = require('config');

module.exports = new require('elasticsearch').Client({
    hosts: [
        {
            http: config.es.protocol,
            host: config.es.hosts.groot,
            port: config.es.port
        }
    ],
    log: config.es.log
});