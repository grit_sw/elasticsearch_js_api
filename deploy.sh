#!/bin/bash

cd /root/elasticsearch_js_api && 
git stash && 
git checkout production && 
git pull && 
docker-compose -f docker-compose.prod.yml up --build -d
