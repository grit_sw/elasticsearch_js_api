/**
 * Sets root level require aliases for requiring in-app(local) modules
 * @param name this describes the name of the module to be required
 */
global.probeModel = name => require(`${__dirname}/src/model/by_probe_id/${name}`);
global.configModel = name => require(`${__dirname}/src/model/by_config_id/${name}`);
global.probeRoute = name => require(`${__dirname}/src/route/by_probe_id/${name}`);
global.configRoute = name => require(`${__dirname}/src/route/by_config_id/${name}`);

/**
 * Load .env variables into node enviroment
 */
require('dotenv').config();
const http = require('http');
const config = require('config');

/**
 * Include app and create server
 */
const app = require('./src/app');
server = http.createServer(app);

server.listen(config.serverPort || 3000, () => {
  console.log(`Server is running. Listening on port ${config.serverPort}`);
});
